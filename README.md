# biofarma


## REQUIREMENT
install yarn and nodemon for development
```
    npm install nodemon yarn -g
```

## installation
install dependencies
```
    yarn install
    yarn prisma generate && yarn prisma migrate dev --name init
```


## Running on local

to start the backend server

```
    yarn devs
```

start react 

```
    yarn dev
```

deploy
```
    yarn build
```

deploy and preview
```
    yarn build && yarn preview
```