import express from "express";
import Prisma from "@prisma/client";
import cors from "cors";
import { encrypt, validate, generateToken } from "./password.js";
import { auth } from "./middleware.js";

const app = express();
const { PrismaClient } = Prisma;
const prisma = new PrismaClient();

app.options("*", cors());
app.use(express.json());

let globalResult = {
  status: false,
  message: "",
  data: {},
};

// CREATE USER
app.post("/create", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  const request = req.body;

  const data = {
    name: request.name,
    email: request.email,
    password: await encrypt(request.password),
  };

  prisma.user
    .create({
      data: data,
    })
    .then(() => {
      globalResult.status = true;
      globalResult.message = "User created successfully";
      return res.json(globalResult);
    })
    .catch(() => {
      globalResult.message = "Email has been used";
      return res.json(globalResult);
    });
});

// LOGIN USER
app.post("/login", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  const request = req.body;
  console.log(request.email);

  let user = await prisma.user.findUnique({
    where: {
      email: request.email,
    }
  });

  if (!user) {
    globalResult.status = false;
    globalResult.message = "Email not found";
    return res.json(globalResult);
  }

  let check = await validate(request.password, user.password);

  if (!check) {
    globalResult.status = false;
    globalResult.message = "Email or password is wrong";
    return res.json(globalResult);
  }

  globalResult.status = true;
  globalResult.message = "Login success";
  globalResult.data = {
    token: await generateToken({
      id: user.id,
      email: user.email,
      name: user.name,
    }),
  };
  return res.json(globalResult);
});

// VERIFY USER TOKEN
app.get("/verify-token", auth, async (req, res) => {
  return res.json({ status: "ok" });
});

export default app;
