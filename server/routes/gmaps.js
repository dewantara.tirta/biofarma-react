import express from "express";
import Prisma from "@prisma/client";
import cors from "cors";
import dotenv from "dotenv";
import { Client } from "@googlemaps/google-maps-services-js";

const app = express();
const { PrismaClient } = Prisma;
const prisma = new PrismaClient();

const { GOOGLE_API_KEY } = dotenv.config().parsed;

app.options("*", cors());
// app.use(express.json());

let globalResult = {
  status: false,
  message: "",
  data: {},
};

const matrixDataManip = (data) => {
  if (data.status !== "OK") {
    globalResult.message = "NO WAYPOINTS";
    globalResult.data = data;
    return globalResult;
  }

  let { routes } = data;
  let newRoutes = [];
  routes.forEach((route, i) => {
    let d = {
      overview_polyline: route.overview_polyline,
      legs: route.legs
    };
    newRoutes.push(d);
  });

  globalResult.status = true;
  globalResult.data = newRoutes;

  return globalResult;
};

app.get("/matrix", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");

  if (Object.keys(req.query).length < 2) {
    globalResult.message = "params not enough";
    return res.json(globalResult);
  }
  const paramsFrom = req.query?.from || false;
  const paramsTo = req.query?.to || false;

  if (!paramsFrom || !paramsTo) {
    globalResult.message = "invalid params from & to";
    return res.json(globalResult);
  }

  const places = await prisma.places.findMany({
    where: {
      id: {
        in: [parseInt(paramsFrom), parseInt(paramsTo)],
      },
    },
  });

  if (places.length < 2) {
    globalResult.message = "invalid id";
    return res.json(globalResult);
  }

  const placeFrom = places.find((place) => place.id === parseInt(paramsFrom));
  const placeTo = places.find((place) => place.id === parseInt(paramsTo));

  const origins = `${placeFrom.latitude},${placeFrom.longitude}`;
  const destinations = `${placeTo.latitude},${placeTo.longitude}`;

  // const origins = [{lat:placeFrom.latitude, lng: placeFrom.longitude}];
  // const destinations = [{lat: placeTo.latitude, lng: placeTo.longitude}];
  console.log(origins);
  console.log(destinations);

  const client = new Client({});
  const directions = await client
    .directions({
      params: {
        origin: origins,
        destination: destinations,
        key: GOOGLE_API_KEY,
        travelMode: "DRIVING",
      },
    })
    .then((result) => {
      const data = matrixDataManip(result.data);
      return res.json(data);
    })
    .catch((err) => {
      console.log(err.message);
      globalResult.data = err;
      return res.json(globalResult);
    });
});

export default app;
