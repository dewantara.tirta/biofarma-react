import bcrypt from "bcrypt";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

const { HASH_SALT } = dotenv.config().parsed;

export const generate_salt = async (round = 10) => {
  return await bcrypt.genSalt(round).then((salt) => {
    return salt;
  });
};

export const encrypt = (password) => {
  return bcrypt.hash(password, HASH_SALT).then((hash) => {
    return hash;
  });
};

export const validate = async (password, hash) => {
  const passHash = await encrypt(password);
  return passHash === hash;
};

export const generateToken = async (payload) => {
  return await jwt.sign(payload, HASH_SALT, { expiresIn: "1h" });
};

export const verifyToken = async (token) => {
  return await jwt.verify(token, HASH_SALT, (err, decoded) => {
    if (err) {
      return false;
    }
    return decoded;
  });
};

export default { encrypt, validate };
