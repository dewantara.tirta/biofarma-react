import express from "express";
import Prisma from "@prisma/client";
import cors from "cors";

const app = express();
const { PrismaClient } = Prisma;
const prisma = new PrismaClient();

app.options("*", cors());
app.use(express.json());

let globalResult = {
  status: false,
  message: "",
  data: {},
};

app.options("*", cors());
app.use(express.json());

app.get("/get-all", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  let data = await prisma.placesType.findMany().then((data) => {
    return data;
  });
  return res.json({ data: data});
});

export default app;
