import express from "express";
import Prisma from "@prisma/client";
import cors from "cors";
import { Graph } from "dijkstra-floydwarshall-graph";
import Constants from "dijkstra-floydwarshall-graph/Graph/Constants.js";

const app = express();
const { PrismaClient } = Prisma;
const prisma = new PrismaClient();

app.options("*", cors());
app.use(express.json());

app.get("/get-all", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  let data = await prisma.places.findMany({
    include: {
      type: true,
    },
  });
  return res.json({ data: data });
});

const toRad = (Value) => {
  return (Value * Math.PI) / 180;
};

//depthSearch wiht starting point myLocation
const depthSearch = (graph, start) => {
  let visited = [];
  let stack = [];
  let totalDistance = 0;
  stack.push(start);

  while (stack.length > 0) {
    let current = stack.pop();
    if (!visited.includes(current)) {
      visited.push(current);
      let neighbors = graph.find((item) => item.id === current).distances;
      neighbors.forEach((item) => {
        totalDistance += item.distance;
        stack.push(item.id);
      });
    }
  }
  return { visited, totalDistance };
};

// Breadth-first search with starting point myLocation
const breadthSearch = (graph, start) => {
  let visited = [];
  let queue = [];
  let totalDistance = 0;
  queue.push(start);

  while (queue.length > 0) {
    let current = queue.shift();
    if (!visited.includes(current)) {
      visited.push(current);
      let neighbors = graph.find((item) => item.id === current).distances;
      neighbors.forEach((item) => {
        
        totalDistance += item.distance;
        queue.push(item.id);
      });
    }
  }
  console.log(visited, queue);
  return { visited, totalDistance };
};

const findShortestRoute = (places, start) => {
  const myLocation = places.find(place => place.id === start);
  const remainingPlaces = places.filter(place => place.id !== start);

  let shortestPath = [];
  let shortestDistance = Infinity;

  function traverse(currentPlace, visited, distance, path) {
    visited.add(currentPlace.id);
    path.push(currentPlace);

    if (visited.size === places.length) {
      if (distance < shortestDistance) {
        shortestDistance = distance;
        shortestPath = path.slice();
      }
    } else {
      for (const distanceObj of currentPlace.distances) {
        const nextPlace = places.find(place => place.id === distanceObj.id);

        if (!visited.has(nextPlace.id)) {
          const nextDistance = distance + distanceObj.distance;
          traverse(nextPlace, new Set(visited), nextDistance, path.slice());
        }
      }
    }
  }

  traverse(myLocation, new Set(), 0, []);

  return {
    path: shortestPath.map(place => place.id),
    distance: shortestDistance,
  };
}

const measureDistance = (lat1, lon1, lat2, lon2) => {
  var R = 6371; // km
  var dLat = toRad(lat2 - lat1);
  var dLon = toRad(lon2 - lon1);
  var vlat1 = toRad(lat1);
  var vlat2 = toRad(lat2);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(vlat1) * Math.cos(vlat2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
};

app.post("/find", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");

  const reqBody = req.body;

  let data = await prisma.places.findMany({
    where: {
      id: {
        in: reqBody.places,
      },
    },
    include: {
      type: true,
    },
  });
  // console.log(req.body.location);
  // console.log(data);
  data.push({
    id: "myLocation",
    latitude: req.body.location[0],
    longitude: req.body.location[1],
  });

  let place = [];

  data.forEach((el, i) => {
    let distances = [];
    data.forEach((el2, x) => {
      if (el.id != el2.id) {
        let distance = measureDistance(
          el.latitude,
          el.longitude,
          el2.latitude,
          el2.longitude
        );

        distances.push({
          id: el2.id,
          distance: distance,
        });
      }
    });
    place.push({ name: el.name, id: el.id, distances: distances });
  });

  const shortest = findShortestRoute(place, 'myLocation')

  return res.json({ data: place, shortest });
});

export default app;
