import express from "express";
import Prisma from "@prisma/client";
import cors from "cors";
import { encrypt, validate, generateToken } from "./password.js";
import { auth } from "./middleware.js";

const app = express();
const { PrismaClient } = Prisma;
const prisma = new PrismaClient();

app.options("*", cors());
app.use(express.json());
app.use(auth);

let globalResult = {
  status: false,
  message: "",
  data: {},
};

// CREATE PLACE
app.post("/create", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  const request = req.body;

  const data = {
    name: request.name,
    address: request.address,
    latitude: parseFloat(request.latitude),
    longitude: parseFloat(request.longitude),
    type: { connect: { id: parseInt(request.type) } },
    author: { connect: { id: parseInt(req.user.id) } },
    city: "",
    state: "",
    phone: "",
    website: "",
    zip: "",
  };

  prisma.places
    .create({
      data: data,
    })
    .then((resp) => {
      globalResult.status = true;
      globalResult.message = "Place created successfully";
      globalResult.data = resp;
      return res.json(globalResult);
    })
    .catch((e) => {
      console.log(e.message);
      globalResult.message = "Failed to create place";
      return res.json(globalResult);
    });
});

app.get("/get-my-places", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  let data = await prisma.places.findMany({
    where: {
      authorId: parseInt(req.user.id),
    },
    include: {
      type: true,
    },
  });
  return res.json({ data: data });
});

app.get("/get/:id", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");

  const place = await prisma.places.findMany({
    where: {
      id: parseInt(req.params.id),
      AND: {
        authorId: parseInt(req.user.id),
      },
    },
  });

  
  if (place.length == 0) {
    globalResult.status = false;
    globalResult.message = "Place not found";
    return res.json(globalResult);
  }

  globalResult.status = true;
  globalResult.data = place[0];
  globalResult.message = "";
  return res.json(globalResult);
});

app.post("/update/:id", async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");

  const place = await prisma.places.findMany({
    where: {
      id: parseInt(req.params.id),
      AND: {
        authorId: parseInt(req.user.id),
      },
    },
  });

  if (place.length == 0) {
    globalResult.status = false;
    globalResult.message = "Place not found";
    return res.json(globalResult);
  }

  const request = req.body;

  return prisma.places.update({
    where: {
      id: parseInt(req.params.id),
    },
    data: {
      name: request.name,
      address: request.address,
      latitude: parseFloat(request.latitude),
      longitude: parseFloat(request.longitude),
      type: { connect: { id: parseInt(request.type) } },
    }
  }).then((resp) => {
      globalResult.status = true;
      globalResult.message = "Place updated successfully";
      globalResult.data = resp;
      return res.json(globalResult);
  })
  .catch((e) => {
    console.log(e.message);
    globalResult.message = "Failed to update place";
    return res.json(globalResult);
  });
});

app.get('/delete/:id', async (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");

  const place = await prisma.places.findMany({
    where: {
      id: parseInt(req.params.id),
      AND: {
        authorId: parseInt(req.user.id),
      },
    },
  });

  if (place.length == 0) {
    globalResult.status = false;
    globalResult.message = "Place not found";
    return res.json(globalResult);
  }

  let rmStatus = await prisma.places.delete({
    where: {
      id: parseInt(req.params.id),
    }
  });

  globalResult.status = true;
  globalResult.message = "Place deleted successfully";
  return res.json(globalResult);
});


export default app;
