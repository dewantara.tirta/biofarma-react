import { verifyToken } from "./password.js";

export const auth = async (req, res, next) => {
  let token = req.headers["authorization"];
  if (!token) {
    return res.send(401);
  }
  token = token.split(" ")[1];

  const verify = await verifyToken(token);
  if (!verify) {
    return res.send(401);
  }

  res.header("Access-Control-Allow-Origin", "*");
  req.user = verify;
  next();
};

export default { auth };
