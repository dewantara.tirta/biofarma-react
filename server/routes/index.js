import express from "express";
import user from "./user.js";
import placetype from "./placetype.js";
import places from "./places.js";
import gmaps from "./gmaps.js";
import findroute from "./findroute.js";

const app = express();


app.use('/user', user);
app.use('/placetype', placetype);
app.use('/places', places);
app.use('/gmaps', gmaps);
app.use('/findroute', findroute);

export default app;
