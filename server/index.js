import express from "express";
import dotenv from "dotenv";
import route from './routes/index.js'

const app = express();
const { API_PORT } = dotenv.config().parsed;

app.use('/api', route )

app.listen(API_PORT, () =>
  console.log(`Server is running on port ${API_PORT}`)
);
