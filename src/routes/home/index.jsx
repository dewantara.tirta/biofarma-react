// import Map from "@/components/maps/Maps";
// import { useNavigate } from "react-router-dom";
import axios from "axios";
import { getToken } from "@lib/token";
import { useEffect, useState, useRef } from "react";
import DraggableMarker from "@/components/maps/DragableMarker";
import CustomMarker from "@components/maps/CustomMarker";
import { Marker, Popup, Polyline } from "react-leaflet";
import { MapContainer, TileLayer } from "react-leaflet";
import Icon36 from "@components/maps/CustomMarker/icons/icon36.svg";
import Icon45 from "@components/maps/CustomMarker/icons/icon45.svg";
import Icon72 from "@components/maps/CustomMarker/icons/icon72.svg";
import Styles from '@/routes/home/styles.css'

const url = import.meta.env.VITE_API_URL;

const getIcon = (type) => {
  switch (type) {
    case 36:
      return Icon36;
    case 45:
      return Icon45;
    case 72:
      return Icon72;
  }
};

const getAllPlace = () => {
  // axios with token
  return axios.get(`${url}/api/findroute/get-all`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  });
};

const findRoute = (data) => {
  let place = [];

  data.places.map((p) => {
    place.push(p.id);
  });
  // place.push({id: 'current-location', location: data.currentLocation});

  return axios
    .post(`${url}/api/findroute/find`, {
      places: place,
      location: data.currentLocation,
    })
    .then((response) => {
      return response;
    });
};

const config = {
  center: [-7.7839193, 110.3664317],
  zoom: 7,
};

const Find = () => {
  const [places, setPlaces] = useState([]);
  const [location, setLocation] = useState(config.center);
  const [selected, setSelected] = useState([]);
  const [result, setResult] = useState("");
  const [totalDistance, setTotalDistance] = useState(0);
  const [polyline, setPolyline] = useState([]);
  const myLocationRef = useRef(null);

  const handleSubmit = (e, currentLocation) => {
    e.preventDefault();

    findRoute({ places: selected, currentLocation }).then((res) => {
      let data = res.data.data;
      let text = ``;
      let totalDistance = 0;
      let polyline = [];
      polyline.push(currentLocation);
      text += "<ul>";
      // console.log(currentLocation);
      res.data.shortest.path.forEach((v, i) => {
        let x = selected.find((s) => s.id == v);
        if (x) {
          polyline.push([x.latitude, x.longitude]);
          let dataBefore = data.find(
            (d) => d.id == res.data.shortest.path[i - 1]
          );
          let dbf = dataBefore.distances.find((ds) => ds.id == v);
          // console.log(dbf);
          text += `<li>${x.id}. ${x.name} - ${
            x.address
          } = ${dbf.distance.toFixed(2)} Km</li>`;
          totalDistance += dbf.distance;
        }
      });
      text += "</ul>";
      setResult(text);
      setTotalDistance(totalDistance);
      setPolyline(polyline);
    });
  };

  const handleSelected = ({ target }) => {
    let value = [];
    for (let i = 0; i < target.options.length; i++) {
      if (target.options[i].selected) {
        let v = places.find(
          (place) => place.id == parseInt(target.options[i].value)
        );
        value.push(v);
      }
    }
    setSelected(value);
  };


  useEffect(() => {
    getAllPlace().then((res) => {
      setPlaces(res.data.data);
    });
  }, []);

  useEffect(() => {}, [selected, result, totalDistance, polyline]);
  if (places.length == 0) return <>Loading...</>;

  return (
    <>
      <h2>Find Shortest Route</h2>
      <small>Traveling Salesman Problem Algorithm</small>
      
      <MapContainer
        center={config.center}
        zoom={13}
        scrollWheelZoom={false}
        id="map"
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <DraggableMarker
          position={location}
          setPosition={setLocation}
          markerRef={myLocationRef}
        />

        {selected.length > 0 &&
          selected.map((place) => (
            <CustomMarker
              key={`placeId-${place.id}`}
              markerPosition={[place.latitude, place.longitude]}
              icon={{
                iconUrl: getIcon(parseInt(place.type.name)),
                iconSize: [36, 80],
              }}
            >
              <Popup>
                {place.name} - {place.address}
              </Popup>
            </CustomMarker>
          ))}
          <Polyline positions={polyline} />
          
      </MapContainer>

      <div className="grid bottom-wrapper">
        <form>
          <label htmlFor="cars">Choose Places:</label>
          <select
            name="cars"
            id="cars"
            onChange={(e) => {
              handleSelected(e);
            }}
            multiple
          >
            {places.map((place) => (
              <option value={place.id} key={place.id}>
                {place.id}. {place.name} - {place.address} [{place.type.name}]
              </option>
            ))}
          </select>

          <button
            type="submit"
            onClick={(e) => {
              handleSubmit(e, location);
            }}
          >
            Find Route
          </button>
        </form>
        <div className="resultWrapper">
          <h3>Result</h3>
          <p>Total Distance: {totalDistance.toFixed(2)} Km</p>
          <div className="html" dangerouslySetInnerHTML={{ __html: result }}></div>
        </div>
      </div>
    </>
  );
};

export default Find;
