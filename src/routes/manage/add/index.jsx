import { redirect, useNavigate } from "react-router-dom";
import { useRef, useEffect, useState } from "react";
import Map from "@/components/maps/Maps";
import axios from "axios";
import DraggableMarker from "@/components/maps/DragableMarker";
import { getToken } from "@lib/token";
const url = import.meta.env.VITE_API_URL;

const onSubmit = (e, form) => {
  e.preventDefault();
  if (!form.reportValidity()) return;

  axios
    .post(
      `${url}/api/places/create`,
      Object.fromEntries(new FormData(form).entries()),
      { headers: { Authorization: `Bearer ${getToken()}` } }
    )
    .then((response) => {
      const { status, message } = response.data;
      if (!status) {
        window.Swal.fire({
          icon: "error",
          title: "Oops...",
          text: message,
        });
      }

      if (status) {
        window.Swal.fire({
          icon: "success",
          title: "Success",
          text: message,
          timer: 1500,
        }).then(() => {
          window.location.href='/manage'
        });
      }
    });
};

const getType = () => {
  const x = axios
    .get(`${url}/api/placetype/get-all`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      removeToken();
      window.location.href = "/login";
      throw e;
    });
  return x;
};

const Add = () => {
  const config = {
    center: [-7.7839193, 110.3664317],
    zoom: 11,
  };
  const navigate = useNavigate();
  const formRef = useRef(null);
  const markerRef = useRef(null);
  const [formData, setFormData] = useState({
    latitude: config.center[0],
    longitude: config.center[1],
  });
  const [position, setPosition] = useState([
    config.center[0],
    config.center[1],
  ]);
  const [type, setType] = useState([]);

  useEffect(() => {
    getType().then((res) => {
      setType(res.data);
    });
  }, []);

  useEffect(() => {
    setFormData({ ...formData, latitude: position[0], longitude: position[1] });
  }, [position]);

  useEffect(() => {
    if (
      formData.latitude !== position[0] ||
      formData.longitude !== position[1]
    ) {
      setPosition([
        parseFloat(formData.latitude),
        parseFloat(formData.longitude),
      ]);
    }
  }, [formData]);

  return (
    <>
      <div className="header-wrapper">
        <h1 className="title">Add new place</h1>
        <a
          href="#"
          onClick={() => {
            navigate("/manage");
          }}
        >
          👈 Back
        </a>
      </div>

      <Map {...config}>
        <DraggableMarker
          markerRef={markerRef}
          center={[
            parseFloat(formData.latitude),
            parseFloat(formData.longitude),
          ]}
          setPosition={setPosition}
          position={position}
        />
      </Map>

      <form ref={formRef}>
        <div className="grid">
          <label htmlFor="name">
            Place Name
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Name"
              value={formData?.name ?? ""}
              onChange={(e) => {
                setFormData({ ...formData, name: e.target.value });
              }}
              required
            />
          </label>
          <label htmlFor="address">
            Address
            <input
              type="text"
              id="address"
              name="address"
              placeholder="Address"
              value={formData?.address ?? ""}
              onChange={(e) => {
                setFormData({ ...formData, address: e.target.value });
              }}
              required
            />
          </label>
        </div>

        <div>
          <label htmlFor="type">Type</label>
          <select
            id="type"
            name="type"
            onChange={(e) => {
              setFormData({ ...formData, type: e.target.value });
            }}
            required
          >
            {type.map((item) => {
              return (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              );
            })}
          </select>
        </div>

        <div className="grid">
          <label htmlFor="latitude">
            Latitude
            <input
              type="number"
              id="latitude"
              name="latitude"
              placeholder="Latitude"
              value={formData?.latitude ?? ""}
              step={0.001}
              onChange={(e) => {
                setFormData({ ...formData, latitude: e.target.value });
              }}
              required
            />
          </label>
          <label htmlFor="longitude">
            Longitude
            <input
              type="number"
              id="longitude"
              name="longitude"
              placeholder="Longitude"
              value={formData?.longitude ?? ""}
              step={0.001}
              onChange={(e) => {
                setFormData({ ...formData, longitude: e.target.value });
              }}
              required
            />
          </label>
        </div>

        <button
          onClick={(e) => {
            onSubmit(e, formRef.current);
          }}
        >
          Submit
        </button>
      </form>
    </>
  );
};

export default Add;
