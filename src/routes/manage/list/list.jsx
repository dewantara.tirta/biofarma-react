import styles from "./styles.css";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { getToken } from "@lib/token";
import { useEffect, useState } from "react";

const url = import.meta.env.VITE_API_URL;

const getMyPlace = () => {
  // axios with token
  return axios.get(`${url}/api/places/get-my-places`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  });
};

const List = (props) => {
  const navigate = useNavigate();
  const [places, setPlaces] = useState([]);

  const handleAdd = (e) => {
    e.preventDefault();
    navigate("/manage/add");
  };

  const editHandler = (id) => {
    navigate(`/manage/edit/${id}`);
  };

  const removeHandler = (id) => {
    axios
      .get(`${url}/api/places/delete/${id}`, {
        headers: { Authorization: `Bearer ${getToken()}` },
      })
      .then((res) => {
        if (res.data.status) {
          window.Swal.fire({
            icon: "success",
            title: "Success",
            text: res.data.message,
            timer: 2000,
          }).then(() => {
            window.location.reload();
          });
        }
      });
  };

  useEffect(() => {
    getMyPlace().then((res) => {
      setPlaces(res.data.data);
    });
  }, []);

  return (
    <>
      <div className="titleWrapper">
        <h1>Your Places</h1>
        <a
          href="#"
          role="button"
          className="button bg-green"
          onClick={handleAdd}
        >
          Add
        </a>
      </div>

      <div className="list">
        {places.length > 0 &&
          places.map((place, idx) => {
            return (
              <div className="col" key={idx}>
                <article>
                  <h5>
                    {place.name}{" "}
                    <kbd className={`kbd-${place.type.name}`}>
                      {place.type.name}
                    </kbd>
                  </h5>
                  <p>{place.address}</p>

                  <abbr
                    title="Latitude / Longitude"
                    data-tooltip="Latitude / Longitude"
                  >
                    {place.latitude} / {place.longitude}
                    <div className="btn-holder">
                      <button
                        className="btn-edit"
                        onClick={() => {
                          editHandler(place.id);
                        }}
                      >
                        Edit
                      </button>
                      <button
                        className="btn-remove"
                        onClick={() => {
                          removeHandler(place.id);
                        }}
                      >
                        Remove
                      </button>
                    </div>
                  </abbr>
                </article>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default List;
