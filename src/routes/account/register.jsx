import { useRef } from "react";
import axios from "axios";
import background from "@assets/login.jpg";
import { Link } from "react-router-dom";

const url = import.meta.env.VITE_API_URL;

const submit = async ({ e, data, form }) => {
  e.preventDefault();

  axios
    .post(
      `${url}/api/user/create`,
      Object.fromEntries(new FormData(data).entries())
    )
    .then((response) => {
      const { status, message } = response.data;
      if (!status) {
        window.Swal.fire({
          icon: "error",
          title: "Oops...",
          text: message,
        });
      }

      if (status) {
        window.Swal.fire({
          icon: "success",
          title: "Success",
          text: message,
          timer: 1500,
        }).then(() => {
          window.location.href = "/account/login";
        });
      }
    });
};

const Reg = () => {
  const form = useRef(null);

  return (
    <div className="flex-center">
      <article className="grid">
        <div
          className="login-bg"
          style={{ backgroundImage: `url(${background})` }}
        />
        <div>
          <hgroup>
            <h1>Register</h1>
            <p>
              {" "}
              Already have account?{" "}
              <Link to="/account/login">
                <a href="/account/login">Login</a>
              </Link>{" "}
              here
            </p>
          </hgroup>
          <form className="formLogin" ref={form}>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Name"
              required
            />
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Email address"
              required
            />
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              required
            />
            <button
              type="submit"
              className="contrast"
              onClick={(e) => {
                submit({ e, data: form.current, form });
              }}
            >
              Login
            </button>
          </form>
        </div>
      </article>
    </div>
  );

};

export default Reg;
