import { useRef } from "react";
import axios from "axios";
import { saveToken } from "@lib/token";
import background from "@assets/login.jpg";
import { Link } from "react-router-dom";

const url = import.meta.env.VITE_API_URL;

const submit = async (e, form) => {
  e.preventDefault();
  const valid = form.reportValidity();
  if (!valid) return;

  axios
    .post(
      `${url}/api/user/login`,
      Object.fromEntries(new FormData(form).entries())
    )
    .then((response) => {
      const { status, message, data } = response.data;
      if (!status) {
        window.Swal.fire({
          icon: "error",
          title: "Oops...",
          text: message,
          timer: 2000,
        }).then(() => {
          form.reset();
        });
      }

      if (status) {
        saveToken(data.token);
        window.Swal.fire({
          icon: "success",
          title: "Success",
          text: message,
          timer: 1500,
        }).then(() => {
          window.location.href = "/";
        });
      }
    });
};

const Reg = () => {
  const form = useRef(null);

  return (
    <div className="flex-center">
      <article className="grid">
        <div>
          <hgroup>
            <h1>Sign in</h1>
            <h2>
              Don't have account?{" "}
              <Link to="/account/register">
                <a href="/account/register">Register</a>
              </Link>{" "}
              here
            </h2>
          </hgroup>
          <form className="formLogin" ref={form}>
            <input
              type="email"
              name="email"
              placeholder="E-mail"
              aria-label="Login"
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              aria-label="Password"
              required
            />
            <button
              type="submit"
              className="contrast"
              onClick={(e) => {
                submit(e, form.current);
              }}
            >
              Login
            </button>
          </form>
        </div>
        <div
          className="login-bg"
          style={{ backgroundImage: `url(${background})` }}
        />
      </article>
    </div>
  );
};

export default Reg;
