import { removeToken } from '@lib/token';
import { redirect } from "react-router-dom";

const DoLogout = () => {
    removeToken();
    // redirect('/account/login');
    window.location.href="/account/login";
    return (null);
};

export default DoLogout;