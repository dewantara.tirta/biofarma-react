const About = () => {
  return (
    <>
      <div>
        <h4>Assessment Biofarma</h4>
        <p>
          Find the shortest route using Traveling Salesman Problem&apos;s
          Algorithm
        </p>
        <h4>Tech Stack</h4>
        <div className="grid">
          <ul>
            <li>
              <a href="https://react.dev/" target="_blank" rel="noreferrer">
                ReactJS
              </a>
            </li>
            <li>
              <a href="https://expressjs.com/" target="_blank" rel="noreferrer">
                Express JS - Backend
              </a>
            </li>
            <li>
              <a
                href="https://www.sqlite.org/index.html"
                target="_blank"
                rel="noreferrer"
              >
                SQLite - Database
              </a>
            </li>
            <li>
              <a href="https://vitejs.dev/" target="_blank" rel="noreferrer">
                Vite - Module Bundler
              </a>
            </li>
          </ul>

          <ul>
            <li>
              <a href="https://www.prisma.io/" target="_blank" rel="noreferrer">
                Prisma - ORM
              </a>
            </li>
            <li>
              <a href="https://leafletjs.com/" target="_blank" rel="noreferrer">
                Leaflet js - Map
              </a>
            </li>
            <li>
              <a href="https://www.cloudflare.com/" target="_blank" rel="noreferrer">
                Cloudflare - DNS
              </a>
            </li>
            <li>
              <a href="https://id.alibabacloud.com/id" target="_blank" rel="noreferrer">
                Alibaba EC - Cloud Hosting
              </a>
            </li>
          </ul>

          <ul>
            <li>
              <a href="https://picocss.com/" target="_blank" rel="noreferrer">
                Pico - CSS Framework
              </a>
            </li>
            <li>
              <a href="https://www.nginx.com/" target="_blank" rel="noreferrer">
                Nginx - Reverse Proxy
              </a>
            </li>
          </ul>

        </div>
      </div>
    </>
  );
};

export default About;
