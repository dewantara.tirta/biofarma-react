import React from "react";
import ReactDOM from "react-dom/client";
import Root from "./routes/home";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
import ErrorPage from "./routes/error-page";
import Layouts from "@/components/layout";
import { Register, Login, Logout } from "./routes/account/index";
import { List, Add, Edit } from "./routes/manage";
import About from './routes/about'

const { Layout, Wrapper } = Layouts;

const router = (props) => {
  delete props.children;

  return createBrowserRouter([
    {
      path: "/",
      element: <Root {...props} />,
      errorElement: <ErrorPage {...props} />,
    },
    {
      path: "/account/register",
      element: <Register {...props} />,
    },
    {
      path: "/account/login",
      element: <Login {...props} />,
    },
    {
      path: "/account/logout",
      element: <Logout {...props} />,
    },
    {
      path: "/manage",
      element: <List {...props} />,
    },
    {
      path: "/manage/add",
      element: <Add {...props} />,
    },
    {
      path: "/manage/edit/:id",
      element: <Edit {...props} />,
    },
    {
      path: "/about",
      element: <About {...props} />,
    },
  ]);
};

export const Wrap = (props) => {
  const { children } = props;
  // return <>{React.cloneElement(children, { ...props })}</>;
  return (
    <>
      <RouterProvider router={router(props)} />
    </>
  );
};

ReactDOM.createRoot(document.getElementById("root")).render(
  <Layout>
    <Wrap/>
  </Layout>
);
