import { Marker, Popup } from "react-leaflet";
import React, { useState, useMemo, useEffect } from "react";
import styles from "./styles.css";

const defaultConfig = {
  center: [-7.7839193, 110.3664317],
  zoom: 12,
};

const DraggableMarker = (props) => {
  const conf = { ...defaultConfig, ...props };
  const [draggable, setDraggable] = useState(true);
  const { position, setPosition } = conf;

  const { markerRef, children } = props;
  if (!markerRef) {
    console.log(`markerRef is not defined`);
    return <></>;
  }

  const eventHandlers = useMemo(
    () => ({
      dragend() {
        const marker = markerRef.current;
        if (marker != null) {
          const position = marker.getLatLng();
          setPosition([position.lat, position.lng]);
        }
      },
    }),
    []
  );

  return (
    <Marker
      draggable={draggable}
      eventHandlers={eventHandlers}
      position={position}
      ref={markerRef}
    >
      {!!children && (
        <Popup minWidth={90}>{React.cloneElement(children, { ...props })}</Popup>
      )}
    </Marker>
  );
};

export default DraggableMarker;
