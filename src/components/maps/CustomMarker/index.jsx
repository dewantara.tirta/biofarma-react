import React from "react";
import { Marker } from "react-leaflet";
import L from "leaflet";
import personIcon from "@components/maps/CustomMarker/icons/person.png";

const getIcon = (props) => {
  const { iconUrl, iconSize, iconAnchor } = props ?? {};
  return L.icon({
    iconUrl: iconUrl ?? personIcon,
    iconSize: iconSize ?? [30, 30],
    iconAnchor: iconAnchor,
  });
};

const CustomMarker = (props) => {
  const { markerPosition, children } = props;
  return (
    <Marker position={markerPosition} icon={getIcon(props?.icon)}>
      {children}
    </Marker>
  );
};

export default CustomMarker;
