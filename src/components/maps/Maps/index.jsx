import { MapContainer, TileLayer, useMap, Marker, Popup } from "react-leaflet";
import { useState, useCallback, useMemo, useRef } from "react";
import styles from "./styles.css";
import React from "react";

const defaultConfig = {
  center: [-7.7839193, 110.3664317],
  zoom: 12,
};

export const Map = (props) => {
  const conf = { ...defaultConfig, ...props };
  const { center, zoom, children } = conf;
  const hasChildren = !!children;

  return (
    <MapContainer center={center} zoom={zoom} scrollWheelZoom={false} id="map">
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {hasChildren ?
        React.cloneElement(children, { ...props.children.props })
        : null
      }
    </MapContainer>
  );
};

export default Map;
