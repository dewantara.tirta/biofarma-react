import Nav from "@layout/nav";
import { isLogin } from "@lib/token";
import React from "react";
import axios from "axios";
import { credentials, removeToken } from "@lib/token";

const url = import.meta.env.VITE_API_URL;

export const Wrapper = (props) => {
  const { children } = props;
  return <>{React.cloneElement(children, { ...props })}</>;
};

const Layout = ({ children }) => {
  const login = isLogin();

  const verifyToken = login
    ? axios
        .get(`${url}/api/user/verify-token`, {
          ...credentials(),
        })
        .then((res) => {
          return res;
        })
        .catch((e) => {
          removeToken();
          window.location.href = "/login";
          throw e;
        })
    : null;

  const props = {
    isLogin: login,
    verifyToken,
  };

  return (
    <>
      <Nav {...props} />
      <main className="container">
        {React.cloneElement(children, { ...props })}
      </main>
    </>
  );
}

export default {
  Layout,
  Wrapper,
};