export const saveToken = (token) => {
  document.cookie = `token=${token}; path=/`;
};

export const getToken = () => {
  const token = document.cookie
    .split(";")
    .filter((item) => item.includes("token"))[0];
  if (token) {
    return token.split("=")[1];
  }
  return null;
};

export const removeToken = () => {
  document.cookie = `token=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT`;
};

export const isLogin = () => {
  return getToken() !== null;
};

export const validateToken = (token) => {
  //axios
};

export const credentials = () => {
  const token = getToken();
  return { headers: { Authorization: `Bearer ${token}` } };
};

export default {
  saveToken,
  getToken,
  removeToken,
  isLogin,
  credentials,
};
